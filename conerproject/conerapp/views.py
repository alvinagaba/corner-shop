from django.shortcuts import render, redirect
from .models import Product, BusinessRegistration
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .forms import SignupForm, BusinessRegistrationForm, UpdateUserForm, ChangePasswordForm, ProductForm
from django.contrib.auth.decorators import login_required



# Create your views here.
def home(request):
    products = Product.objects.all()
    return render(request,'index.html',{'products':products})
def contacts(request):
    return render(request,'contacts.html')

def about(request):
    return render(request,'about.html')

def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, ('You have been logged in!!!!'))
            return redirect('/')
        else:
            messages.success(request, ('There was an error please try again---'))
            return redirect('login')
    else:
        return render(request,'login.html')

def logout_user(request):
    logout(request)
    messages.success(request, ('You have been logged out successfully!!!!!!!'))
    return redirect('home')

def register_user(request):
    form = SignupForm()
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, ('You have been Registered successfully!!!!!!!'))
            return redirect('/')
        else:
            messages.success(request, ('Try again!!!!!!!'))
            return redirect('register')
    else:
        form.fields['password1'].help_text = ""
        return render(request, 'register.html', {'form':form})
    
@login_required(login_url ='login')
def business_register(request):
    if request.method =="POST":
        forma = BusinessRegistrationForm(request.POST)
        if forma.is_valid():
            forma.save()
            return redirect('/')
    else:
        forma = BusinessRegistrationForm()
        return render(request, 'business.html', {'forma':forma})
    
def update_user(request):
    if request.user.is_authenticated:
        current_user = User.objects.get(id=request.user.id)
        user_form = UpdateUserForm(request.POST or None, instance=current_user)

        if user_form.is_valid():
            user_form.save()

            login(request, current_user)
            messages.success(request, 'User has been udated!!!!')
            return redirect('home')
        return render(request, 'update_user.html', {'user_form':user_form})
        
    else:
        messages.success(request, 'Try again!!!!')
        return redirect('home')
    

def update_password(request):
    if request.user.is_authenticated:
      current_user = request.user

      if request.method == "POST":
          form = ChangePasswordForm(current_user, request.POST)
          if form.is_valid():
              form.save()
              messages.success(request, 'your password has been update!!!')

          else:
              for error in list(form.errors.values()):
                  messages.error(request,error)
                  return redirect('update_password')

    

      else:
        form = ChangePasswordForm(current_user)
        form.fields['new_password1'].help_text = ""

        return render(request, 'update_password.html',{'form':form})
    
    else:
        messages.success(request, 'You must be logged in!!!!')
        return redirect('home')

        
@login_required(login_url ='login')
def product_add(request):
    if request.method =="POST":
        form = ProductForm(request.POST)
        if form.is_valid():
            return redirect('/')
            if request.user.BusinessRegistration:
                product = form.save
                product.user = request.user
                product.save()
            else:
                return render(request, 'product.html',{'form':form})
        else:
            return redirect('business')
    else:
        
        return render(request, 'product.html', )

      
      
    

