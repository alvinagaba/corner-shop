from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, SetPasswordForm
from django import forms
from .models import BusinessRegistration, Product, Category

class SignupForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password1','password2']

    first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}) , required=True)
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}), required=True)
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}), required=True)
    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'input'}), required=True)
    password1 = forms.EmailField(widget=forms.PasswordInput(attrs={'class':'input'}), required=True)
    password2 = forms.EmailField(widget=forms.PasswordInput(attrs={'class':'input'}), required=True)



    
    

    
class BusinessRegistrationForm(forms.ModelForm):
    class Meta:
        model= BusinessRegistration
        fields = ['business_name', 'email', 'contact',  'bussiness_registration_number', 'address','description']

 

    business_name = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}),max_length=30)
    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'input'}))
    contact = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}),max_length=25)
   
    bussiness_registration_number = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}),max_length=25)
    address = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}),max_length=30)
    description = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}),max_length = 300)

class UpdateUserForm(UserChangeForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email']
    password = None
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}) , required=True)
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}), required=True)
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'input'}), required=True)
    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'input'}), required=True)
    

class ChangePasswordForm(SetPasswordForm):
    class Meta:
        model = User
        fields = ['password1', 'password2']

        password1 = forms.EmailField(widget=forms.PasswordInput(attrs={'class':'input'}), required=True)
        password2 = forms.EmailField(widget=forms.PasswordInput(attrs={'class':'input'}), required=True)



class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['category', 'name', 'description', 'price','image', 'is_sold']
    category = forms.CharField( max_length=25)
    name = forms.CharField(max_length=25)
    description = forms.CharField( max_length=25)
    price = forms.IntegerField()
    image = forms.ImageField()
    is_sold = forms.BooleanField()